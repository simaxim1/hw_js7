"use strict"

/* 
    1. Опишіть своїми словами як працює метод forEach.

        Він дозволяє працювати з кожним елементом масиву за допомогою функції (перебирає елементи масиву).
        Нічого не повертає.

    2. Як очистити масив?

        Накращій спосіб - arr.lenght = 0;

    3. Як можна перевірити, що та чи інша змінна є масивом?

        За допомогою вбудованого методу Array.isArray(), за аналогією isFinite, Number.isInteger()....
        Також, думаю, можна спробувати через пошук індексу, тому що навіть для порожнього масиву arr.lenght = 0;
*/
           
const array = ['hello', 'world', 1, 2, true, false, null, undefined, {}, [], 45n];

const filterBy = (arr,type) => arr.filter((item) => typeof(item) !== type);

console.log(filterBy(array,'number'));